package net.chameleoncreepermod.client;

import net.chameleoncreepermod.common.ChameleonCreeperMod;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.color.world.BiomeColors;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.feature.FeatureRenderer;
import net.minecraft.client.render.entity.feature.FeatureRendererContext;
import net.minecraft.client.render.entity.model.CreeperEntityModel;
import net.minecraft.client.render.entity.model.EntityModelLoader;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.mob.CreeperEntity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;

public class ChameleonCreeperFeatureRenderer extends FeatureRenderer<CreeperEntity, CreeperEntityModel<CreeperEntity>> {
    private static final Identifier TEXTURE = new Identifier(ChameleonCreeperMod.MODID, "textures/entity/creeper/greyscale_creeper.png");
    private final CreeperEntityModel<CreeperEntity> entityModel;

    @SuppressWarnings("unused")
    public ChameleonCreeperFeatureRenderer(FeatureRendererContext<CreeperEntity, CreeperEntityModel<CreeperEntity>> featureRendererContext, EntityModelLoader loader) {
        super(featureRendererContext);
        entityModel = featureRendererContext.getModel();
    }

    @Override
    public void render(MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, CreeperEntity entity, float limbAngle, float limbDistance, float tickDelta, float animationProgress, float headYaw, float headPitch) {
        float[] tint = getBlockBiomeColors(entity);
        render(getContextModel(), entityModel, TEXTURE, matrices, vertexConsumers, light, entity, limbAngle, limbDistance, animationProgress,
                headYaw, headPitch, tickDelta, tint[0], tint[1], tint[2]);
    }

    // ripped from the BiomeColors class in ChameleonCreeper mod source and slightly modified to suit the chameleoncreeper and FabricMC
    // credit for this chameleoncreeper color changing algorithm goes to Nikos creator of the ChameleonCreeper mod
    public static float[] getBlockBiomeColors(MobEntity creeper) {
        Vec3d v = creeper.getPos();
        double entityX = v.x;
        double entityY = v.y;
        double entityZ = v.z;

        int red = 0;
        int green = 0;
        int blue = 0;

        int currColor;
        int blockCount = 27; // 3 * 3 * 3

        for (int x = -1; x <= 1; x++) {
            for (int y = 0; y <= 2; ++y) {
                for (int z = -1; z <= 1; z++) {
                    BlockPos bp = new BlockPos(entityX + x, entityY + y - 0.5, entityZ + z);
                    BlockState iBlockState = creeper.world.getBlockState(bp);
                    Block block = iBlockState.getBlock();

                    if (creeper.world.getBlockState(bp).isAir()) {
                        blockCount--;
                        continue;
                    }

                    if (block == Blocks.GRASS || block == Blocks.TALL_GRASS) {
                        currColor = BiomeColors.getGrassColor(creeper.world, new BlockPos(entityX + x, entityY + y, entityZ + z)); // grass
                    } else if (isLeaves(block)) {
                        currColor = BiomeColors.getFoliageColor(creeper.world, new BlockPos(entityX + x, entityY + y, entityZ + z)); // foliage
                    } else if (block == Blocks.WATER) {
                        currColor = BiomeColors.getWaterColor(creeper.world, new BlockPos(entityX + x, entityY + y, entityZ + z)); // water
                    } else {
                        currColor = iBlockState.getMaterial().getColor().color; // other blocks
                    }
                    red += (currColor & 0xFF0000) >> 16;
                    green += (currColor & 0xFF00) >> 8;
                    blue += currColor & 0xFF;
                }
            }
        }

        // default to brown color if the Chameleon is falling through the air (without any blocks near it)
        // also has the benefit of avoiding a divide by zero
        if (blockCount == 0) {
            return new float[]{139, 131, 120};
        }

        red /= blockCount;
        green /= blockCount;
        blue /= blockCount;
        float f = 0.003925686274509801f; // (1.0 / 255.0)

        return new float[] {red * f, green * f, blue  * f};
    }

    private static boolean isLeaves(Block block) {
        return block == Blocks.ACACIA_LEAVES |
                block == Blocks.BIRCH_LEAVES |
                block == Blocks.DARK_OAK_LEAVES |
                block == Blocks.JUNGLE_LEAVES |
                block == Blocks.OAK_LEAVES |
                block == Blocks.SPRUCE_LEAVES;
    }

}
