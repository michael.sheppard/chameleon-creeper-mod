package net.chameleoncreepermod.mixin;

import net.chameleoncreepermod.client.ChameleonCreeperFeatureRenderer;
import net.minecraft.client.render.entity.CreeperEntityRenderer;
import net.minecraft.client.render.entity.EntityRendererFactory.Context;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.render.entity.feature.CreeperChargeFeatureRenderer;
import net.minecraft.client.render.entity.model.CreeperEntityModel;
import net.minecraft.client.render.entity.model.EntityModelLayers;
import net.minecraft.entity.mob.CreeperEntity;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;


@Mixin(CreeperEntityRenderer.class)
public abstract class CreeperRendererMixin extends MobEntityRenderer<CreeperEntity, CreeperEntityModel<CreeperEntity>> {
    public CreeperRendererMixin(Context context) {
        super(context, new CreeperEntityModel<>(context.getPart(EntityModelLayers.CREEPER)), 0.5F);
        addFeature(new CreeperChargeFeatureRenderer(this, context.getModelLoader()));
    }

    @Inject(at = @At("TAIL"), method = "<init>")
    private void addCreeperFeatureRenderer(Context context, CallbackInfo info) {
        addFeature(new ChameleonCreeperFeatureRenderer(this, context.getModelLoader()));
    }
}
